import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-servidores',
  // selector: '.app-servidores', //class='app-servidores'
  // selector: '[app-servidores]', //<div  app-servidores></div>
  templateUrl: './servidores.component.html',
  styleUrls: ['./servidores.component.css']
})
export class ServidoresComponent implements OnInit {

  allowNewServer = false;
  serverCreationStatus = 'No server creation';
  serverName = 'TestServer';
  serverCreate = false;

  constructor() {

    setTimeout(() => {
      this.allowNewServer = true
    }, 2000)
  }

  onCreateServer() {
    this.serverCreate = true;
    this.serverCreationStatus = 'servidor creado!!!!'+this.serverName;
  }

  //creamos una funcion para obtener el valor de un input
  onUpdateServername(evento: Event) {
    this.serverName = (<HTMLInputElement>evento.target).value;
  }

  ngOnInit() {
  }

}
