/**
 * Created by yacel100 on 11/2/18.
 */

import {Component} from '@angular/core';
@Component({
  selector: 'app-server',
  templateUrl: './server.component.html'
})
export class ServerComponent {

  serverId: number = 10;
  serverStatus: string = 'offline';
}
