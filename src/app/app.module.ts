import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
//agregando al el componente server
import {ServerComponent} from "./server/server.component";
import { ServidoresComponent } from './servidores/servidores.component';

@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    ServidoresComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
